import { Observable } from 'rxjs/Observable'; 

export function getMocks() {
    const mocks = {
        actionService: {
            list: () => Observable.of([]),
        },
        badgeService: {
            list: () => Observable.of([]),
            myBadges: () => Observable.of([]),
        },
        userService: {
            getUser: () => {},
            userChangeEvent: Observable.of({}),
        },
        globalState: {
            subscribe: () => Observable.of({}),
        },
        restangular: {
            one: () => { 
                return {};
            },
            all: () => {
                return { getList: () => Observable.of({}) };
            },
            
        },
        dialogRef: {
            afterClosed: () => Observable.of(true),
            file: '',
            onDelete: Observable.of({}),
            onRename: Observable.of({}),
        },
        dialog: {            
            open: () => {
                return { componentInstance: mocks.dialogRef };
            },
        },
        snackBar: {
            open: () => {},
        },
    };
    return mocks;
}
