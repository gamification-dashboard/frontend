import { Component, Inject, OnInit } from '@angular/core';
import { ObjectiveService } from '../services/objective.service';
import { Objective } from '../models/objective';

@Component({
  selector: 'objectives',
  templateUrl: './objectives.html',
  styleUrls: ['./objectives.scss'],
  providers: [ObjectiveService],  
})
export class ObjectivesComponent {

  objectives: Objective[];

  constructor( private objectiveService: ObjectiveService ) {
   
    const params = { 'page': 1, 'per_page': 50 };
    this.objectiveService.list(params).subscribe(
      objectives => {
          this.objectives = objectives;
      },
    );    
  }
}
