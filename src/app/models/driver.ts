import { Objective } from '../models/objective';

export class Driver {
    
    id: number;
    title: string;
    description: string;    
    objective: Objective;
    objective_id: number;
    
}
