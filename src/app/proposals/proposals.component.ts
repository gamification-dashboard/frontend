import { Component, Inject, Input, OnInit } from '@angular/core';
import { ProposalService } from '../services/proposal.service';
import { ObjectiveService } from '../services/objective.service';
import { Objective } from '../models/objective';
import { Proposal } from '../models/proposal';
import { ProposalModalComponent } from './proposal-modal/proposal-modal.component';
import { ProposalShowModalComponent } from './proposal-show-modal/proposal-show-modal.component';
import { ActivatedRoute } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { MessageNotificationService } from '../services/message-notification.service';
import { User } from '../models/user';
import { UserService } from '../services/user.service';

import * as _ from 'lodash';

@Component({
  selector: 'proposals',
  templateUrl: './proposals.html',
  styleUrls: ['./proposals.scss'],
  providers: [ProposalService, NgbModalRef, ObjectiveService, MessageNotificationService],  
})
export class ProposalsComponent {

  @Input() objective: Objective;
  proposals: Proposal[];
  ngbModalRef: NgbModalRef;
  page = 1;
  user: User;
  
  constructor( private proposalService: ProposalService, private route: ActivatedRoute, 
    private modalService: NgbModal, private objectiveService: ObjectiveService, 
    private messageNotificationService: MessageNotificationService, private userService: UserService  ) {
    this.proposals = [];
    this.user = this.userService.getUser();    
    
    
    this.route.params.subscribe( params => {
      // if(!_.isObject(this.objective) || (this.objective.id !=params['objective'] )){
      //   this.objective = <Objective>{id: params['objective']};
      //   this.loadProposals();

      // };

      this.objectiveService.get(params['objective']).subscribe(
        objective => {
          this.objective = objective;
          this.page = 1;
          this.loadProposals();
        },
      ); 
    });
  }

  vote(proposal: Proposal){
    this.proposalService.vote(proposal).subscribe( (new_proposal) => {
      console.log('voce votou na proposta ', proposal)
      proposal = Object.assign(proposal, new_proposal);
      this.messageNotificationService.success({ title: "", message: "Seu voto foi salvo com sucesso" });
    }, (error) =>{
      console.log(error);
      this.messageNotificationService.error({ title: "", message: 'Ocorreu um erro ao tentar salvar seu voto e ele não foi salvo.'});
    }); 
  }

  addNewProposal(){
    
    this.ngbModalRef = this.modalService.open(ProposalModalComponent, {size: 'lg'});
    let proposal = new Proposal();
    proposal.objective = this.objective;
    this.ngbModalRef.componentInstance.proposal = proposal;

    this.ngbModalRef.componentInstance.onSave.subscribe((wasSaved) => {
      this.messageNotificationService.success({ title: "", message: "Proposta salva com sucesso" });
      this.loadProposals();
    });
  }

  editProposal(proposal: Proposal){
    
    this.ngbModalRef = this.modalService.open(ProposalModalComponent, {size: 'lg'});
    this.ngbModalRef.componentInstance.proposal = proposal;
    this.ngbModalRef.componentInstance.onSave.subscribe((value) => {
      if(_.isNil(value)){
        this.messageNotificationService.success({ title: "", message: 'Sua proposta foi salva com sucesso!'});
      }else{
        this.messageNotificationService.error({ title: "", message: _.join(value['already_voted'], ' ')});
      }
    });
  }

  displayProposal(proposal: Proposal){
    if(_.isNil(proposal.objective)){
      proposal.objective = this.objective;
    }
    this.ngbModalRef = this.modalService.open(ProposalShowModalComponent, {size: 'lg'});
    this.ngbModalRef.componentInstance.proposal = proposal;
  }

  nextPage(){
    this.page += 1;
    this.loadProposals({page: this.page});
  }
  previousPage(){
    this.page -= 1;
    this.loadProposals({page: this.page});
  }

  isPreviousActive(){
    return this.page > 1;
  }
  isNextActive(){
    return _.size(this.proposals) > 0;
  }

  isOwner(proposal: Proposal){
    return proposal.user_id === this.user.id;
  }

  alreadyVoted(proposal: Proposal){
    return _.includes(proposal.voting_users_ids, this.user.id);
  }
  
  loadProposals(params = {}){
    const default_params = { 'page': 1, 'per_page': 10 };
    params = Object.assign(default_params, params);
    this.proposalService.list(this.objective, params).subscribe(
      proposals => {
          this.proposals = proposals;
      },
    ); 
  }



}
