import { Component } from '@angular/core';
import { RankingService } from '../services/ranking.service';
import { User } from '../models/user';
import * as _ from 'lodash';

@Component({
  selector: 'ranking-users',
  templateUrl: './ranking-users.html',
  styleUrls: ['./ranking-users.scss'],
  providers: [RankingService],  
})
export class RankingUsersComponent {
  users: User[];
  page = 1;
  
  constructor( private rankingService: RankingService ) {      

    this.loadUsers();
    
  }

  nextPage(){
    this.page += 1;
    this.loadUsers({page: this.page});
  }
  previousPage(){
    this.page -= 1;
    this.loadUsers({page: this.page});
  }

  isPreviousActive(){
    return this.page > 1;
  }
  isNextActive(){
    return _.size(this.users) > 0;
  }

  loadUsers(params = {}){
    const default_params = { 'page': 1, 'per_page': 20 };
    params = Object.assign(default_params, params);
    this.rankingService.usersRanking({per_page: 50}).subscribe(
      users => {        
          this.users = users;
      },
    );   
  }

}
