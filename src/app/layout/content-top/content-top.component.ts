import { Component } from '@angular/core';

import { GlobalState } from '../../global.state';

@Component({
  selector: 'layout-content-top',
  styleUrls: ['./content-top.scss'],
  templateUrl: './content-top.html',
})
export class ContentTopComponent {

  activePageTitle: string = '';

  constructor(private _state: GlobalState) {
    this._state.subscribe('menu.activeLink', (activeLink) => {
      if (activeLink) {
        this.activePageTitle = activeLink.title;
      }
    });
  }
}
