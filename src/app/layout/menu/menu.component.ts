import { Component, Input, Output, EventEmitter, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import * as _ from 'lodash';

import { MenuItem } from '../../models/menu-item';
import { ObjectiveService } from '../../services/objective.service';
import { Objective } from '../../models/objective';

@Component({
  selector: 'layout-menu',
  templateUrl: './menu.html',
  styleUrls: ['./menu.scss'],
  providers: [ObjectiveService],    
})
export class MenuComponent {

  @Input() sidebarCollapsed: boolean = false;
  @Input() menuHeight: number;
 
  @Output() expandMenu = new EventEmitter<any>();

  menuItems: MenuItem[];
  objectives: Objective[];
  
  hoverElemHeight: number;
  hoverElemTop: number;
  
  constructor(private _router: Router, private objectiveService: ObjectiveService) {
    this.menuItems = [];
    this.menuItems.push(this.createMenuItem({ title: 'general.menu.begin', icon: 'ion-android-home', path: ['/dashboard'] }));
    // this.menuItems.push(this.createMenuItem({ title: 'general.menu.actions', icon: 'ion-compose', path: ['/dashboard'] }));
    // this.menuItems.push(<MenuItem>{ title: 'general.menu.actions', icon: 'ion-compose', selected: false, path: ['/actions'] });
    this.menuItems.push(<MenuItem>{ title: 'general.menu.users', icon: 'ion-android-people', selected: false, path: ['/users'] });
    this.menuItems.push(<MenuItem>{ title: 'general.menu.proposals', icon: 'ion-android-create', selected: false, path: ['/proposals'] });
    this.menuItems.push(this.createMenuItem({ title: 'general.menu.badges', icon: '', path: ['/badges'] }));

    this.objectiveService.list().subscribe(objectives => {
      for (let objective of objectives) {
        let objectiveMenu = this.createMenuItem({ title: objective.title, path: ['/objectives/' + objective.id + '/proposals'] })
        // objectiveMenu.children = [];
        // for (let driver of objective.drivers) {
        //   // objectiveMenu.children.push(this.createMenuItem({ title: driver.title, path: ['/objectives/'+ objective.id + '/drivers/'+ driver.id] + '/proposals'}));
        //   objectiveMenu.children.push(this.createMenuItem({ title: driver.title, path: ['/drivers/'+ driver.id]}));
        // }          
        this.menuItems.push(objectiveMenu);
      }
    });
  }

  createMenuItem(params = {}): MenuItem {
    let menuItem = new MenuItem;
    menuItem.title = '';
    menuItem.icon = 'ion-compose';
    menuItem.selected = false;
    menuItem.hidden = false;
    menuItem.expanded = false;
    menuItem.path = [''];
    return Object.assign(menuItem, params);
  }

  hoverItem($event): void {
    this.hoverElemHeight = $event.currentTarget.clientHeight;
    // TODO: get rid of magic 66 constant
    this.hoverElemTop = $event.currentTarget.getBoundingClientRect().top - 76;
  }

  public toggleSubMenu($event): boolean {
    let submenu = jQuery($event.currentTarget).next();

    if (this.sidebarCollapsed) {
      this.expandMenu.emit(null);
      if (!$event.item.expanded) {
        $event.item.expanded = true;
      }
    } else {
      $event.item.expanded = !$event.item.expanded;
      submenu.slideToggle();
    }

    return false;
  }

}
