import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, fakeAsync, tick, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { TranslateModule } from '@ngx-translate/core';
import * as helpers from '../../../spec/helpers';
import { Observable } from 'rxjs/Observable'; 

import { BadgeCardComponent } from './badge-card.component';
import { BadgeService } from '../../services/badge.service';

describe('BadgeCardComponent', () => {

  let component: BadgeCardComponent;
  let fixture: ComponentFixture<BadgeCardComponent>;
  const mocks = helpers.getMocks();
  
  beforeEach(async(() => {    
    TestBed.configureTestingModule({
      declarations: [BadgeCardComponent],
      providers: [{ provide: BadgeService, useValue: mocks.badgeService }],
      imports: [TranslateModule.forRoot()],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).overrideComponent(BadgeCardComponent, {
      set: {
        providers: [
          { provide: BadgeService, useValue: mocks.badgeService }],
    }});

    fixture = TestBed.createComponent(BadgeCardComponent);
    component = fixture.componentInstance;
  }));

  it('should display the list of badges', () => {
    expect(fixture.debugElement.queryAll(By.css('.badges-list')).length).toEqual(1);
  });

});
