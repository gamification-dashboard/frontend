import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, fakeAsync, tick, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { TranslateModule } from '@ngx-translate/core';
import * as helpers from '../../../spec/helpers';
import { Observable } from 'rxjs/Observable'; 

import { RankingProposalsCardComponent } from './ranking-proposals-card.component';
import { BadgeService } from '../../services/badge.service';

describe('RankingProposalsCardComponent', () => {

  let component: RankingProposalsCardComponent;
  let fixture: ComponentFixture<RankingProposalsCardComponent>;
  const mocks = helpers.getMocks();
  
  // beforeEach(async(() => {    
  //   TestBed.configureTestingModule({
  //     declarations: [RankingProposalsCardComponent],
  //     providers: [{ provide: BadgeService, useValue: mocks.badgeService }],
  //     imports: [TranslateModule.forRoot()],
  //     schemas: [CUSTOM_ELEMENTS_SCHEMA],
  //   }).overrideComponent(RankingProposalsCardComponent, {
  //     set: {
  //       providers: [
  //         { provide: BadgeService, useValue: mocks.badgeService }],
  //   }});

  //   fixture = TestBed.createComponent(RankingProposalsCardComponent);
  //   component = fixture.componentInstance;
  // }));

  // it('should display the list of ranked users', () => {
  //   expect(fixture.debugElement.queryAll(By.css('.ranking-list')).length).toEqual(1);
  // });

});
