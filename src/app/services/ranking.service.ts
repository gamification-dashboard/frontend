import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Restangular } from 'ngx-restangular';
import * as _ from 'lodash';

@Injectable()
export class RankingService {

  constructor ( private restangular: Restangular ) {}

  usersRanking (params?: any): Observable<any> {        
    if (!_.isObject(params)) {
      params = {};
    }

    params['page'] = params['page'] || 1;
    params['per_page'] = params['per_page'] || 10;    

    return this.restangular.one('ranking').all('users').getList(params);
  }

  proposalsRanking (params?: any): Observable<any> {        
    if (!_.isObject(params)) {
      params = {};
    }

    params['page'] = params['page'] || 1;
    params['per_page'] = params['per_page'] || 10;    

    return this.restangular.one('ranking').all('proposals').getList(params);
  }

}
