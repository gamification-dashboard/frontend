import { Injectable } from '@angular/core';
import { Objective } from '../models/objective';
import { Observable } from 'rxjs/Observable';
import { Restangular } from 'ngx-restangular';

import * as _ from 'lodash';

@Injectable()
export class ObjectiveService {

  constructor ( private restangular: Restangular) {}

  list (params?: any): any {        
    if (!_.isObject(params)) {
      params = {};
    }

    params['page'] = params['page'] || 1;
    params['per_page'] = params['per_page'] || 5;  
    return this.restangular.all('strategic_objectives').getList();
  }

  get (id: number): any {        
    return this.restangular.one('strategic_objectives', id).get();
  }
}
