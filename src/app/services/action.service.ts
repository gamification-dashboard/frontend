import { Injectable } from '@angular/core';
import { Action } from '../models/action';
import { Observable } from 'rxjs/Observable';
import { Restangular } from 'ngx-restangular';
import * as _ from 'lodash';

@Injectable()
export class ActionService {

  constructor ( private restangular: Restangular ) {}

  list (params?: any): Observable<Action[]> {        
    if (!_.isObject(params)) {
      params = {};
    }

    params['page'] = params['page'] || 1;
    params['per_page'] = params['per_page'] || 5;    
    return this.restangular.all('activities').getList(params);
  }

}
